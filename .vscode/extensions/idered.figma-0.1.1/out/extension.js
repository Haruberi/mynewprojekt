"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const vscode = require("vscode");
const constants_1 = require("./constants");
const figma_tree_view_1 = require("./figma-tree-view");
const create_store_1 = require("./utils/create-store");
function activate(context) {
    const store = create_store_1.createStore(context);
    const tree = new figma_tree_view_1.FigmaTreeView(context, store);
    let panel = undefined;
    function getWebviewContent({ fileId, fileName, nodeId }) {
        return `
      <!DOCTYPE html>
      <html lang="en">
        <head>
          <meta charset="UTF-8" />
          <meta name="viewport" content="width=device-width, initial-scale=1.0" />
          <title>Cat Coding</title>
        </head>
        <body style="padding: 0;">
          <iframe
            src="https://www.figma.com/embed?embed_host=share&url=https://www.figma.com/file/${fileId}/${fileName}?node-id=${nodeId}"
            allowfullscreen
          ></iframe>
          <style>
            iframe {
              border: none;
              width: 100vw;
              height: 100vh;
            }
          </style>
        </body>
      </html>
    `;
    }
    vscode.commands.registerCommand("extension.copyCSS", (style) => __awaiter(this, void 0, void 0, function* () {
        const editor = vscode.window.activeTextEditor;
        if (editor) {
            editor.edit(editBuilder => {
                editBuilder.replace(editor.selection, style.toCSS(editor.selection.start.character));
            });
        }
    }));
    // FIGMA: CONNECT
    context.subscriptions.push(vscode.commands.registerCommand("figma.connect", () => __awaiter(this, void 0, void 0, function* () {
        let token = yield vscode.window.showInputBox({
            placeHolder: "Figma Personal Access Token",
            value: context.globalState.get(constants_1.STORAGE_KEYS.TOKEN)
        });
        if (token) {
            store.setToken(token);
            tree.provider.refresh();
        }
    })));
    // FIGMA: VIEW LAYER
    context.subscriptions.push(vscode.commands.registerCommand("figma.viewLayer", (node) => __awaiter(this, void 0, void 0, function* () {
        if (panel) {
            // If we already have a panel, show it in the target column
            panel.reveal();
        }
        else {
            // Otherwise, create a new panel
            panel = vscode.window.createWebviewPanel("figma", // Identifies the type of the webview. Used internally
            "Figma Preview", // Title of the panel displayed to the user
            vscode.ViewColumn.Two, {
                enableScripts: true
            });
            // Reset when the current panel is closed
            panel.onDidDispose(() => {
                panel = undefined;
            }, null, context.subscriptions);
        }
        if (panel && store.file && store.fileId && node.id) {
            panel.webview.html = getWebviewContent({
                fileId: store.fileId,
                fileName: store.file.name,
                nodeId: node.id
            });
        }
    })));
    // FIGMA: ADD TEAM
    context.subscriptions.push(vscode.commands.registerCommand("figma.addTeam", () => __awaiter(this, void 0, void 0, function* () {
        let name = yield vscode.window.showInputBox({ placeHolder: "Team name" });
        if (!name)
            return;
        let id = yield vscode.window.showInputBox({ placeHolder: "Team ID" });
        if (!id)
            return;
        store.addTeam({ name, id });
        tree.provider.refresh();
    })));
    // FIGMA: SELECT FILE
    context.subscriptions.push(vscode.commands.registerCommand("figma.selectFile", () => __awaiter(this, void 0, void 0, function* () {
        let pick;
        // Choose team
        pick = yield vscode.window.showQuickPick(store.teams.map(item => ({ id: item.id, label: item.name })), {
            placeHolder: "Choose a team"
        });
        if (pick === undefined)
            return;
        store.setTeam(pick.id);
        // Load projects
        yield vscode.window.withProgress({
            title: "Loading projects...",
            location: vscode.ProgressLocation.Notification
        }, () => store.loadProjects());
        // Choose project
        pick = yield vscode.window.showQuickPick(store.projects.map(item => ({ label: item.name, id: item.id })), {
            placeHolder: "Choose a project"
        });
        if (pick === undefined)
            return;
        store.setProject(pick.id);
        // Load project files
        yield vscode.window.withProgress({
            title: "Loading project files...",
            location: vscode.ProgressLocation.Notification
        }, () => store.loadProjectFiles());
        // Choose project file
        pick = yield vscode.window.showQuickPick(store.projectFiles.map(item => ({ label: item.name, id: item.key })), {
            placeHolder: "Choose a project file"
        });
        if (pick === undefined)
            return;
        store.setProjectFile(pick.id);
        // Load selected project file
        yield vscode.window.withProgress({
            title: "Loading project file...",
            location: vscode.ProgressLocation.Notification
        }, () => store.loadFile());
        store.loadStyles().then(() => {
            tree.provider.refresh();
        });
    })));
}
exports.activate = activate;
function deactivate() { }
exports.deactivate = deactivate;
//# sourceMappingURL=extension.js.map