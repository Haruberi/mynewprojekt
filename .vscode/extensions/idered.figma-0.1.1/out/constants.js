"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.STORAGE_KEYS = {
    FILE: "figma-file",
    TOKEN: "figma-api-token",
    TEAMS: "figma-teams"
};
exports.FIGMA_API = "https://api.figma.com/v1/";
//# sourceMappingURL=constants.js.map