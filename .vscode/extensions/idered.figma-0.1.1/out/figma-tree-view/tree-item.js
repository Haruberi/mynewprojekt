"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const vscode = require("vscode");
class FigmaTreeItem extends vscode.TreeItem {
    constructor(props) {
        super(props.label || "undefined label", props.collapsibleState);
        this.id = props.id;
        this.command = props.command;
        this.iconPath = props.iconPath;
        this.description = props.description;
        this.contextValue = props.contextValue;
    }
    get tooltip() {
        return `${this.label}`;
    }
}
exports.FigmaTreeItem = FigmaTreeItem;
//# sourceMappingURL=tree-item.js.map