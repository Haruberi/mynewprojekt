"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const vscode = require("vscode");
const provider_1 = require("./provider");
class FigmaTreeView {
    constructor(context, store) {
        this.provider = new provider_1.FigmaTreeDataProvider(context, store);
        this.view = vscode.window.createTreeView("figmaView", {
            treeDataProvider: this.provider
        });
    }
}
exports.FigmaTreeView = FigmaTreeView;
//# sourceMappingURL=tree-view.js.map