"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.addToken = {
    id: "placeholder.addToken",
    label: "Add Figma Personal Access Token",
    command: {
        title: "Add token",
        command: "figma.connect"
    }
};
exports.addTeam = {
    id: "placeholder.addTeam",
    label: "Add team",
    command: {
        title: "Add team",
        command: "figma.addTeam"
    }
};
exports.selectFile = {
    id: "placeholder.selectFile",
    label: "Select file",
    command: {
        title: "Select file",
        command: "figma.selectFile"
    }
};
//# sourceMappingURL=tree-items.js.map