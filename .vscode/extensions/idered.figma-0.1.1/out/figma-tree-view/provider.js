"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const vscode = require("vscode");
const tree_item_1 = require("./tree-item");
const treeItems = require("./tree-items");
const traverse = require("traverse");
function getCollapsibleStateForItem(item) {
    return item.children === undefined || item.children.length === 0
        ? vscode.TreeItemCollapsibleState.None
        : vscode.TreeItemCollapsibleState.Collapsed;
}
class FigmaTreeDataProvider {
    constructor(context, store) {
        this._onDidChangeTreeData = new vscode.EventEmitter();
        this.onDidChangeTreeData = this._onDidChangeTreeData
            .event;
        this.getChildrenForNode = (pageId) => {
            if (this.store.file === undefined)
                return Promise.resolve([]);
            let children = [];
            traverse(this.store.file.document.children.slice().reverse()).forEach(function (item) {
                if (item && item.id === pageId) {
                    if (item.children === undefined)
                        return this.stop();
                    item.children
                        .slice()
                        .reverse()
                        .forEach((child) => {
                        children.push({
                            id: child.id,
                            parentId: item.id,
                            label: child.name,
                            collapsibleState: getCollapsibleStateForItem(child),
                            contextValue: `${child.type}_${this.level}`
                        });
                    });
                    this.stop();
                }
            });
            return Promise.resolve(children);
        };
        this.getChildrenForPages = () => {
            if (this.store.file === undefined)
                return Promise.resolve([]);
            const pages = this.store.file.document.children.map(item => ({
                id: item.id,
                label: item.name,
                contextValue: item.type,
                collapsibleState: item.children === undefined || item.children.length === 0
                    ? vscode.TreeItemCollapsibleState.None
                    : vscode.TreeItemCollapsibleState.Collapsed
            }));
            return Promise.resolve(pages);
        };
        this.getChildrenForStyles = () => {
            if (this.store.file === undefined)
                return Promise.resolve([]);
            const styles = [...this.store.file.styles.values()].map(style => {
                let iconPath;
                if (style.styleType === "FILL" &&
                    style.props &&
                    style.props.fills &&
                    style.props.fills[0] &&
                    style.props.fills[0].color) {
                    iconPath = style.props.fills[0].color.uri;
                }
                return {
                    id: `${style.key}-${style.name}`,
                    label: style.name,
                    description: style.styleType,
                    iconPath,
                    command: {
                        title: "Copy CSS",
                        arguments: [style],
                        command: "extension.copyCSS",
                        tooltip: " Copy CSS Tooltip"
                    }
                };
            });
            styles.sort((a, b) => {
                if (a.description !== b.description) {
                    return ("" + a.description).localeCompare(String(b.description));
                }
                return ("" + a.label).localeCompare(String(b.label));
            });
            return Promise.resolve(styles);
        };
        this.context = context;
        this.store = store;
    }
    refresh() {
        this._onDidChangeTreeData.fire();
    }
    getTreeItem(element) {
        return new tree_item_1.FigmaTreeItem(element);
    }
    getParent(element) {
        let parent;
        if (this.store.file === undefined)
            return Promise.resolve(undefined);
        if (element.parentId === undefined)
            return Promise.resolve(undefined);
        traverse(this.store.file.document.children).forEach(function (item) {
            if (item.id === element.parentId) {
                parent = item;
                this.stop();
            }
        });
        return Promise.resolve(parent);
    }
    getChildren(element) {
        if (this.store.token === undefined) {
            return Promise.resolve([treeItems.addToken]);
        }
        if (this.store.teams.length === 0) {
            return Promise.resolve([treeItems.addTeam]);
        }
        if (this.store.file === undefined) {
            return Promise.resolve([treeItems.selectFile]);
        }
        if (element === undefined) {
            return Promise.resolve(this.getTopLevelItems());
        }
        if (element.contextValue === "styles") {
            return this.getChildrenForStyles();
        }
        if (element.contextValue === "pages") {
            return this.getChildrenForPages();
        }
        return this.getChildrenForNode(element.id);
    }
    getTopLevelItems() {
        return [
            {
                id: "pages",
                label: "Pages",
                contextValue: "pages",
                collapsibleState: vscode.TreeItemCollapsibleState.Expanded
            },
            {
                id: "styles",
                label: "Styles",
                contextValue: "styles",
                collapsibleState: vscode.TreeItemCollapsibleState.Expanded
            }
        ];
    }
}
exports.FigmaTreeDataProvider = FigmaTreeDataProvider;
//# sourceMappingURL=provider.js.map