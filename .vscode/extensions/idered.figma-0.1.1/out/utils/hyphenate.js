"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.hyphenate = (s) => s.replace(/([A-Z])/g, g => `-${g[0].toLowerCase()}`).replace(/^ms-/, "-ms-");
//# sourceMappingURL=hyphenate.js.map