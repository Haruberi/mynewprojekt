"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const axios_1 = require("axios");
const mobx_state_tree_1 = require("mobx-state-tree");
const constants_1 = require("../constants");
function figma(self, path, config) {
    const store = mobx_state_tree_1.getRoot(self);
    return axios_1.default.get(`${constants_1.FIGMA_API}${path}`, Object.assign({}, config, { headers: Object.assign({}, (config ? config.headers : {}), { "X-FIGMA-TOKEN": store.token }) }));
}
exports.figma = figma;
//# sourceMappingURL=figma.js.map