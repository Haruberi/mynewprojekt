"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const hyphenate_1 = require("./hyphenate");
exports.objectToCss = (css, indent = 0) => Object.entries(css)
    .filter(([, value]) => value !== undefined)
    .reduce((all, [key, value]) => `${all ? `${all}\n${" ".repeat(indent)}` : ""}${[hyphenate_1.hyphenate(key)]}: ${value};`, "");
//# sourceMappingURL=object-to-css.js.map