"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const store_1 = require("../store/store");
const constants_1 = require("../constants");
const mobx_state_tree_1 = require("mobx-state-tree");
const mst_middlewares_1 = require("mst-middlewares");
function createStore(context) {
    const store = store_1.Store.create({
        file: context.globalState.get(constants_1.STORAGE_KEYS.FILE),
        token: context.globalState.get(constants_1.STORAGE_KEYS.TOKEN),
        teams: context.globalState.get(constants_1.STORAGE_KEYS.TEAMS)
    });
    mobx_state_tree_1.addMiddleware(store, mst_middlewares_1.simpleActionLogger);
    mobx_state_tree_1.onSnapshot(store, snapshot => {
        context.globalState.update(constants_1.STORAGE_KEYS.TOKEN, snapshot.token);
        context.globalState.update(constants_1.STORAGE_KEYS.TEAMS, snapshot.teams);
    });
    return store;
}
exports.createStore = createStore;
//# sourceMappingURL=create-store.js.map