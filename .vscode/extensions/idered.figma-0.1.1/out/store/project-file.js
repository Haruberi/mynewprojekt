"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mobx_state_tree_1 = require("mobx-state-tree");
exports.ProjectFile = mobx_state_tree_1.types.model("ProjectFile", {
    key: mobx_state_tree_1.types.identifier,
    name: mobx_state_tree_1.types.string,
    thumbnail_url: mobx_state_tree_1.types.string,
    last_modified: mobx_state_tree_1.types.string
});
//# sourceMappingURL=project-file.js.map