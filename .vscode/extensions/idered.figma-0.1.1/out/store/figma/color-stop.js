"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mobx_state_tree_1 = require("mobx-state-tree");
const color_1 = require("./color");
exports.ColorStop = mobx_state_tree_1.types.model("ColorStop", {
    position: mobx_state_tree_1.types.number,
    color: color_1.Color
});
//# sourceMappingURL=color-stop.js.map