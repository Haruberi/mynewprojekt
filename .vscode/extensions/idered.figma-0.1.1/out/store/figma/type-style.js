"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mobx_state_tree_1 = require("mobx-state-tree");
const paint_1 = require("./paint");
const TextCaseType = mobx_state_tree_1.types.enumeration("TextCase", ["ORIGINAL", "UPPER", "LOWER", "TITLE"]);
const TextDecorationType = mobx_state_tree_1.types.enumeration("TextDecoration", [
    "NONE",
    "STRIKETHROUGH",
    "UNDERLINE"
]);
const TextAlignHorizontalType = mobx_state_tree_1.types.enumeration("TextAlignHorizontal", [
    "LEFT",
    "RIGHT",
    "CENTER",
    "JUSTIFIED"
]);
const TextAlignVerticalType = mobx_state_tree_1.types.enumeration("TextAlignVertical", ["TOP", "CENTER", "BOTTOM"]);
const LineHeightUnitType = mobx_state_tree_1.types.enumeration("LineHeightUnit", [
    "PIXELS",
    "FONT_SIZE_%",
    "INTRINSIC_%"
]);
exports.TypeStyle = mobx_state_tree_1.types
    .model("TypeStyle", {
    fontFamily: mobx_state_tree_1.types.maybe(mobx_state_tree_1.types.string),
    fontPostScriptName: mobx_state_tree_1.types.maybeNull(mobx_state_tree_1.types.string),
    paragraphSpacing: mobx_state_tree_1.types.maybe(mobx_state_tree_1.types.number),
    paragraphIndent: mobx_state_tree_1.types.maybe(mobx_state_tree_1.types.number),
    italic: mobx_state_tree_1.types.maybe(mobx_state_tree_1.types.boolean),
    fontWeight: mobx_state_tree_1.types.maybe(mobx_state_tree_1.types.number),
    fontSize: mobx_state_tree_1.types.maybe(mobx_state_tree_1.types.number),
    textCase: mobx_state_tree_1.types.maybe(TextCaseType),
    textDecoration: mobx_state_tree_1.types.maybe(TextDecorationType),
    textAlignHorizontal: mobx_state_tree_1.types.maybe(TextAlignHorizontalType),
    textAlignVertical: mobx_state_tree_1.types.maybe(TextAlignVerticalType),
    letterSpacing: mobx_state_tree_1.types.maybe(mobx_state_tree_1.types.number),
    fills: mobx_state_tree_1.types.array(paint_1.Paint),
    lineHeightPx: mobx_state_tree_1.types.maybe(mobx_state_tree_1.types.number),
    lineHeightPercent: mobx_state_tree_1.types.maybe(mobx_state_tree_1.types.number),
    lineHeightPercentFontSize: mobx_state_tree_1.types.maybe(mobx_state_tree_1.types.number),
    lineHeightUnit: mobx_state_tree_1.types.maybe(LineHeightUnitType)
})
    .views(self => ({
    toCSS() {
        return {
            "letter-spacing": self.letterSpacing ? `${self.letterSpacing.toFixed(3)}em` : undefined,
            "vertical-align": self.textAlignVertical ? self.textAlignVertical.toLowerCase() : undefined,
            "text-align": self.textAlignHorizontal ? self.textAlignHorizontal.toLowerCase() : undefined,
            "font-family": self.fontFamily,
            "font-style": self.italic ? "italic" : "normal",
            "font-size": `${self.fontSize}px`,
            "font-weight": self.fontWeight || "normal",
            "line-height": self.lineHeightUnit
                ? {
                    "FONT_SIZE_%": self.lineHeightPercentFontSize,
                    "INTRINSIC_%": self.lineHeightPercent,
                    PIXELS: self.lineHeightPx
                }[self.lineHeightUnit]
                : undefined
        };
    }
}));
//# sourceMappingURL=type-style.js.map