"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mobx_state_tree_1 = require("mobx-state-tree");
const blend_mode_1 = require("./blend-mode");
const node_type_1 = require("./node-type");
const color_1 = require("./color");
const rectangle_1 = require("./rectangle");
const layout_constraint_1 = require("./layout-constraint");
const paint_1 = require("./paint");
const effect_1 = require("./effect");
const type_style_1 = require("./type-style");
const export_settings_1 = require("./export-settings");
const vector_1 = require("./vector");
const transform_1 = require("./transform");
const layout_grid_1 = require("./layout-grid");
const BooleanOperationType = mobx_state_tree_1.types.enumeration("BooleanOperation", ["UNION", "INTERSECT", "SUBTRACT", "EXCLUDE"]);
const StrokeAlignType = mobx_state_tree_1.types.enumeration("StrokeAlign", ["INSIDE", "OUTSIDE", "CENTER"]);
const StrokeCapType = mobx_state_tree_1.types.enumeration("StrokeCap", ["NONE", "ROUND", "SQUARE", "LINE_ARROW", "TRIANGLE_ARROW"]);
const StrokeJoinType = mobx_state_tree_1.types.enumeration("StrokeJoin", ["MITTER", "BEVEL", "ROUND"]);
exports.Node = mobx_state_tree_1.types.model("Node", {
    absoluteBoundingBox: mobx_state_tree_1.types.maybe(rectangle_1.Rectangle),
    background: mobx_state_tree_1.types.maybe(mobx_state_tree_1.types.array(paint_1.Paint)),
    backgroundColor: mobx_state_tree_1.types.maybe(color_1.Color),
    blendMode: mobx_state_tree_1.types.maybe(blend_mode_1.BlendMode),
    booleanOperation: mobx_state_tree_1.types.maybe(BooleanOperationType),
    characters: mobx_state_tree_1.types.maybe(mobx_state_tree_1.types.string),
    characterStyleOverrides: mobx_state_tree_1.types.maybe(mobx_state_tree_1.types.array(mobx_state_tree_1.types.number)),
    children: mobx_state_tree_1.types.maybe(mobx_state_tree_1.types.array(mobx_state_tree_1.types.late(() => exports.Node))),
    clipsContent: mobx_state_tree_1.types.maybe(mobx_state_tree_1.types.boolean),
    componentId: mobx_state_tree_1.types.maybe(mobx_state_tree_1.types.string),
    constraints: mobx_state_tree_1.types.maybe(layout_constraint_1.LayoutConstraint),
    cornerRadius: mobx_state_tree_1.types.maybe(mobx_state_tree_1.types.number),
    effects: mobx_state_tree_1.types.maybe(mobx_state_tree_1.types.array(effect_1.Effect)),
    exportSettings: mobx_state_tree_1.types.maybe(mobx_state_tree_1.types.array(export_settings_1.ExportSetting)),
    fills: mobx_state_tree_1.types.maybe(mobx_state_tree_1.types.array(paint_1.Paint)),
    id: mobx_state_tree_1.types.string,
    isMask: mobx_state_tree_1.types.maybe(mobx_state_tree_1.types.boolean),
    isMaskOutline: mobx_state_tree_1.types.maybe(mobx_state_tree_1.types.boolean),
    layoutGrids: mobx_state_tree_1.types.maybe(mobx_state_tree_1.types.array(layout_grid_1.LayoutGrid)),
    name: mobx_state_tree_1.types.string,
    opacity: mobx_state_tree_1.types.maybe(mobx_state_tree_1.types.number),
    preserveRatio: mobx_state_tree_1.types.maybe(mobx_state_tree_1.types.boolean),
    prototypeStartNodeID: mobx_state_tree_1.types.maybeNull(mobx_state_tree_1.types.string),
    rectangleCornerRadii: mobx_state_tree_1.types.maybe(mobx_state_tree_1.types.array(mobx_state_tree_1.types.number)),
    relativeTransform: mobx_state_tree_1.types.maybe(transform_1.Transform),
    size: mobx_state_tree_1.types.maybe(vector_1.Vector),
    strokeAlign: mobx_state_tree_1.types.maybe(StrokeAlignType),
    strokeCap: mobx_state_tree_1.types.maybe(StrokeCapType),
    strokeDashes: mobx_state_tree_1.types.maybe(mobx_state_tree_1.types.array(mobx_state_tree_1.types.number)),
    strokeJoin: mobx_state_tree_1.types.maybe(StrokeJoinType),
    strokeMiterAngle: mobx_state_tree_1.types.maybe(mobx_state_tree_1.types.number),
    strokes: mobx_state_tree_1.types.maybe(mobx_state_tree_1.types.array(paint_1.Paint)),
    style: mobx_state_tree_1.types.maybe(type_style_1.TypeStyle),
    styleOverrideTable: mobx_state_tree_1.types.maybe(mobx_state_tree_1.types.map(type_style_1.TypeStyle)),
    styles: mobx_state_tree_1.types.maybe(mobx_state_tree_1.types.map(mobx_state_tree_1.types.string)),
    type: node_type_1.NodeType
});
//# sourceMappingURL=node.js.map