"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mobx_state_tree_1 = require("mobx-state-tree");
exports.EasingType = mobx_state_tree_1.types.enumeration("EasingType", ["EASE_IN", "EASE_OUT", "EASE_IN_AND_OUT"]);
//# sourceMappingURL=easing-type.js.map