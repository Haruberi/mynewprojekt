"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mobx_state_tree_1 = require("mobx-state-tree");
const paint_1 = require("./paint");
const effect_1 = require("./effect");
const type_style_1 = require("./type-style");
const object_to_css_1 = require("../../utils/object-to-css");
const layout_grid_1 = require("./layout-grid");
const StyleProps = mobx_state_tree_1.types.model("StyleProps", {
    layoutGrids: mobx_state_tree_1.types.maybe(mobx_state_tree_1.types.array(layout_grid_1.LayoutGrid)),
    fills: mobx_state_tree_1.types.maybe(mobx_state_tree_1.types.array(paint_1.Paint)),
    effects: mobx_state_tree_1.types.maybe(mobx_state_tree_1.types.array(effect_1.Effect)),
    style: mobx_state_tree_1.types.maybe(type_style_1.TypeStyle)
});
exports.Style = mobx_state_tree_1.types
    .model("Style", {
    key: mobx_state_tree_1.types.maybe(mobx_state_tree_1.types.string),
    name: mobx_state_tree_1.types.string,
    styleType: mobx_state_tree_1.types.enumeration("StyleType", [
        "FILL",
        "TEXT",
        "EFFECT",
        "GRID"
    ]),
    props: mobx_state_tree_1.types.maybe(StyleProps)
})
    .actions(self => ({
    setProps(props) {
        self.props = mobx_state_tree_1.cast(props);
    }
}))
    .views(self => ({
    toCSS(indent = 0) {
        let css = {};
        if (self.props === undefined)
            return "";
        if (self.styleType === "GRID" && self.props.layoutGrids) {
            css = self.props.layoutGrids
                .map(item => item.toCSS())
                .reduce((all, current) => (Object.assign({}, all, current)));
        }
        if (self.styleType === "FILL" && self.props.fills) {
            css = self.props.fills
                .map(item => item.toCSS())
                .reduce((all, current) => (Object.assign({}, all, current)));
        }
        if (self.styleType === "TEXT" && self.props.style) {
            css = self.props.style.toCSS();
        }
        return object_to_css_1.objectToCss(css, indent);
    }
}));
//# sourceMappingURL=style.js.map