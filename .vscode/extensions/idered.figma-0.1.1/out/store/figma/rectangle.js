"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mobx_state_tree_1 = require("mobx-state-tree");
exports.Rectangle = mobx_state_tree_1.types.model("Rectangle", {
    x: mobx_state_tree_1.types.number,
    y: mobx_state_tree_1.types.number,
    width: mobx_state_tree_1.types.number,
    height: mobx_state_tree_1.types.number
});
//# sourceMappingURL=rectangle.js.map