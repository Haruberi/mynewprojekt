"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mobx_state_tree_1 = require("mobx-state-tree");
const color_1 = require("./color");
exports.LayoutGrid = mobx_state_tree_1.types
    .model("LayoutGrid", {
    pattern: mobx_state_tree_1.types.enumeration("LayoutGridPattern", [
        "COLUMNS",
        "ROWS",
        "GRID"
    ]),
    sectionSize: mobx_state_tree_1.types.number,
    visible: mobx_state_tree_1.types.boolean,
    color: color_1.Color,
    alignment: mobx_state_tree_1.types.enumeration("LayoutGridAlignment", [
        "MIN",
        "STRETCH",
        "CENTER"
    ]),
    gutterSize: mobx_state_tree_1.types.number,
    offset: mobx_state_tree_1.types.number,
    count: mobx_state_tree_1.types.number
})
    .views(self => ({
    toCSS() {
        return Object.assign({ display: "grid" }, {
            COLUMNS: {
                "column-gap": `${self.gutterSize}px`,
                "grid-template-columns": `repeat(${self.count}, ${self.sectionSize}px)`
            },
            ROWS: {
                "row-gap": `${self.gutterSize}px`,
                "grid-template-rows": `repeat(${self.count}, ${self.sectionSize}px)`
            },
            GRID: {}
        }[self.pattern]);
    }
}));
//# sourceMappingURL=layout-grid.js.map