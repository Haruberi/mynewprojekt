"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mobx_state_tree_1 = require("mobx-state-tree");
exports.Component = mobx_state_tree_1.types.model("Component", {
    key: mobx_state_tree_1.types.string,
    name: mobx_state_tree_1.types.string,
    description: mobx_state_tree_1.types.string
});
//# sourceMappingURL=component.js.map