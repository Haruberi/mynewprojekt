"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mobx_state_tree_1 = require("mobx-state-tree");
const color_1 = require("./color");
const blend_mode_1 = require("./blend-mode");
const vector_1 = require("./vector");
const color_stop_1 = require("./color-stop");
const transform_1 = require("./transform");
const ScaleModeType = mobx_state_tree_1.types.enumeration("PaintScaleMode", ["FILL", "FIT", "TILE", "STRETCH"]);
exports.Paint = mobx_state_tree_1.types
    .model("Paint", {
    type: mobx_state_tree_1.types.enumeration("PaintType", [
        "SOLID",
        "GRADIENT_LINEAR",
        "GRADIENT_RADIAL",
        "GRADIENT_ANGULAR",
        "GRADIENT_DIAMOND",
        "IMAGE",
        "EMOJI"
    ]),
    visible: mobx_state_tree_1.types.maybe(mobx_state_tree_1.types.boolean),
    opacity: mobx_state_tree_1.types.maybe(mobx_state_tree_1.types.number),
    color: mobx_state_tree_1.types.maybe(color_1.Color),
    blendMode: blend_mode_1.BlendMode,
    gradientHandlePositions: mobx_state_tree_1.types.array(vector_1.Vector),
    gradientStops: mobx_state_tree_1.types.array(color_stop_1.ColorStop),
    scaleMode: mobx_state_tree_1.types.maybe(ScaleModeType),
    imageTransform: transform_1.Transform,
    scalingFactor: mobx_state_tree_1.types.maybe(mobx_state_tree_1.types.number),
    imageRef: mobx_state_tree_1.types.maybe(mobx_state_tree_1.types.string)
})
    .views(self => ({
    toCSS() {
        return {
            opacity: self.opacity ? self.opacity.toFixed(2) : undefined,
            color: self.color ? self.color.value : undefined
        };
    }
}));
//# sourceMappingURL=paint.js.map