"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mobx_state_tree_1 = require("mobx-state-tree");
exports.NodeType = mobx_state_tree_1.types.enumeration("NodeType", [
    "DOCUMENT",
    "CANVAS",
    "FRAME",
    "INSTANCE",
    "RECTANGLE",
    "GROUP",
    "TEXT",
    "VECTOR",
    "BOOLEAN_OPERATION",
    "STAR",
    "LINE",
    "ELLIPSE",
    "REGULAR_POLYGON",
    "SLICE",
    "COMPONENT"
]);
//# sourceMappingURL=node-type.js.map