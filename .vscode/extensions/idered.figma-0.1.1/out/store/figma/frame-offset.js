"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mobx_state_tree_1 = require("mobx-state-tree");
const vector_1 = require("./vector");
exports.FrameOffset = mobx_state_tree_1.types.model("FrameOffset", {
    nodeId: mobx_state_tree_1.types.string,
    nodeOffset: vector_1.Vector
});
//# sourceMappingURL=frame-offset.js.map