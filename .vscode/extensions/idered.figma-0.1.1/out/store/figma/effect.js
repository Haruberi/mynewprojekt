"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mobx_state_tree_1 = require("mobx-state-tree");
const color_1 = require("./color");
const blend_mode_1 = require("./blend-mode");
const vector_1 = require("./vector");
exports.Effect = mobx_state_tree_1.types.model("Effect", {
    type: mobx_state_tree_1.types.enumeration("EffectType", ["INNER_SHADOW", "DROP_SHADOW", "LAYER_BLUR", "BACKGROUND_BLUR"]),
    visible: mobx_state_tree_1.types.boolean,
    radius: mobx_state_tree_1.types.number,
    color: mobx_state_tree_1.types.maybe(color_1.Color),
    blendMode: mobx_state_tree_1.types.maybe(blend_mode_1.BlendMode),
    offset: mobx_state_tree_1.types.maybe(vector_1.Vector)
});
//# sourceMappingURL=effect.js.map