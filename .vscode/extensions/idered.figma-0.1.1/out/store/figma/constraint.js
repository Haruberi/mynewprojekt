"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mobx_state_tree_1 = require("mobx-state-tree");
exports.Constraint = mobx_state_tree_1.types.model("Constraint", {
    type: mobx_state_tree_1.types.enumeration("ConstraintType", ["SCALE", "WIDTH", "HEIGHT"]),
    value: mobx_state_tree_1.types.number
});
//# sourceMappingURL=constraint.js.map