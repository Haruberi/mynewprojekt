"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mobx_state_tree_1 = require("mobx-state-tree");
const constraint_1 = require("./constraint");
exports.ExportSetting = mobx_state_tree_1.types.model("ExportSetting", {
    suffix: mobx_state_tree_1.types.string,
    format: mobx_state_tree_1.types.string,
    constraint: constraint_1.Constraint
});
//# sourceMappingURL=export-settings.js.map