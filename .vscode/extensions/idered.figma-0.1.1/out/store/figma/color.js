"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mobx_state_tree_1 = require("mobx-state-tree");
const rgb_to_hex_1 = require("../../utils/rgb-to-hex");
const vscode = require("vscode");
exports.Color = mobx_state_tree_1.types
    .model("Color", {
    r: mobx_state_tree_1.types.number,
    g: mobx_state_tree_1.types.number,
    b: mobx_state_tree_1.types.number,
    a: mobx_state_tree_1.types.number
})
    .views(self => ({
    get hex() {
        const parse = (value) => Math.round(value * 255);
        return rgb_to_hex_1.rgbToHex(parse(self.r), parse(self.g), parse(self.b));
    },
    get value() {
        const parse = (value) => Math.round(value * 255);
        return `rgba(${parse(self.r)}, ${parse(self.g)}, ${parse(self.b)}, 1)`;
    }
}))
    .views(self => ({
    get uri() {
        return vscode.Uri.parse(`https://via.placeholder.com/16/${self.hex}/${self.hex}`).with({
            scheme: "https"
        });
    }
}));
//# sourceMappingURL=color.js.map