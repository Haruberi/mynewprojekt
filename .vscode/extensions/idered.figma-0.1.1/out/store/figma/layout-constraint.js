"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mobx_state_tree_1 = require("mobx-state-tree");
exports.LayoutConstraint = mobx_state_tree_1.types.model("LayoutConstraint", {
    vertical: mobx_state_tree_1.types.enumeration("LayoutConstraintVertical", ["TOP", "BOTTOM", "CENTER", "TOP_BOTTOM", "SCALE"]),
    horizontal: mobx_state_tree_1.types.enumeration("LayoutConstraintHorizontal", ["LEFT", "RIGHT", "CENTER", "LEFT_RIGHT", "SCALE"])
});
//# sourceMappingURL=layout-constraint.js.map