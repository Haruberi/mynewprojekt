"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mobx_state_tree_1 = require("mobx-state-tree");
exports.Vector = mobx_state_tree_1.types.model("Vector", {
    x: mobx_state_tree_1.types.number,
    y: mobx_state_tree_1.types.number
});
//# sourceMappingURL=vector.js.map