"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mobx_state_tree_1 = require("mobx-state-tree");
const node_type_1 = require("./node-type");
const node_1 = require("./node");
exports.Document = mobx_state_tree_1.types.model("Document", {
    id: mobx_state_tree_1.types.string,
    name: mobx_state_tree_1.types.string,
    type: node_type_1.NodeType,
    children: mobx_state_tree_1.types.array(node_1.Node)
});
//# sourceMappingURL=document.js.map