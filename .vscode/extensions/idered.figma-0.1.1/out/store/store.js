"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const mobx_state_tree_1 = require("mobx-state-tree");
const team_1 = require("./team");
const project_1 = require("./project");
const project_file_1 = require("./project-file");
const file_1 = require("./file");
const figma_1 = require("../utils/figma");
exports.Store = mobx_state_tree_1.types
    .model("Store", {
    token: mobx_state_tree_1.types.maybe(mobx_state_tree_1.types.string),
    file: mobx_state_tree_1.types.maybe(file_1.File),
    fileId: mobx_state_tree_1.types.maybe(mobx_state_tree_1.types.string),
    projectFile: mobx_state_tree_1.types.maybe(mobx_state_tree_1.types.reference(project_file_1.ProjectFile)),
    projectFiles: mobx_state_tree_1.types.array(project_file_1.ProjectFile),
    project: mobx_state_tree_1.types.maybe(mobx_state_tree_1.types.reference(project_1.Project)),
    projects: mobx_state_tree_1.types.array(project_1.Project),
    team: mobx_state_tree_1.types.maybe(mobx_state_tree_1.types.reference(team_1.Team)),
    teams: mobx_state_tree_1.types.array(team_1.Team)
})
    .actions(self => ({
    setToken(token) {
        self.token = token;
    },
    addTeam(team) {
        self.teams.push(team);
    },
    setTeam(team) {
        self.team = mobx_state_tree_1.cast(team);
    },
    setProjects(projects) {
        self.projects = mobx_state_tree_1.cast(projects);
    },
    setProject(project) {
        self.project = mobx_state_tree_1.cast(project);
    },
    setProjectFiles(projectFiles) {
        self.projectFiles = mobx_state_tree_1.cast(projectFiles);
    },
    setProjectFile(projectFile) {
        self.projectFile = mobx_state_tree_1.cast(projectFile);
    },
    setFile(file) {
        self.file = mobx_state_tree_1.cast(file);
    },
    setFileId(id) {
        self.fileId = id;
    }
}))
    .actions(self => ({
    loadStyles() {
        return __awaiter(this, void 0, void 0, function* () {
            if (self.projectFile !== undefined && self.file !== undefined) {
                try {
                    const response = yield figma_1.figma(self, `files/${self.projectFile.key}/nodes`, {
                        params: { ids: [...self.file.styles.keys()].join(",") }
                    });
                    Array.from(self.file.styles.entries()).forEach(([nodeId, node]) => {
                        const relatedNode = response.data.nodes[nodeId];
                        if (relatedNode === undefined)
                            return {};
                        node.setProps({
                            layoutGrids: relatedNode.document.layoutGrids,
                            fills: relatedNode.document.fills,
                            effects: relatedNode.document.effects,
                            style: relatedNode.document.style
                        });
                    });
                }
                catch (err) { }
            }
            return undefined;
        });
    },
    loadProjects: () => __awaiter(this, void 0, void 0, function* () {
        if (self.team === undefined)
            return;
        try {
            const response = yield figma_1.figma(self, `teams/${self.team.id}/projects`);
            console.log({ response });
            self.setProjects(response.data.projects);
            return self.projects;
        }
        catch (err) {
            console.log(err);
        }
    }),
    loadProjectFiles: () => __awaiter(this, void 0, void 0, function* () {
        if (self.project === undefined)
            return;
        const response = yield figma_1.figma(self, `projects/${self.project.id}/files`);
        self.setProjectFiles(response.data.files);
        return self.projectFiles;
    }),
    loadFile() {
        return __awaiter(this, void 0, void 0, function* () {
            if (self.projectFile === undefined)
                return;
            const response = yield figma_1.figma(self, `files/${self.projectFile.key}`);
            self.setFile(response.data);
            self.setFileId(self.projectFile.key);
            return self.file;
        });
    }
}));
//# sourceMappingURL=store.js.map