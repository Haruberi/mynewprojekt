"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mobx_state_tree_1 = require("mobx-state-tree");
exports.Team = mobx_state_tree_1.types.model("Team", {
    id: mobx_state_tree_1.types.identifier,
    name: mobx_state_tree_1.types.string
});
//# sourceMappingURL=team.js.map