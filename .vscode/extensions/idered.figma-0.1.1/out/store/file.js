"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mobx_state_tree_1 = require("mobx-state-tree");
const component_1 = require("./figma/component");
const style_1 = require("./figma/style");
const document_1 = require("./figma/document");
exports.File = mobx_state_tree_1.types.model("File", {
    document: document_1.Document,
    name: mobx_state_tree_1.types.string,
    schemaVersion: mobx_state_tree_1.types.number,
    components: mobx_state_tree_1.types.map(component_1.Component),
    styles: mobx_state_tree_1.types.map(style_1.Style),
    lastModified: mobx_state_tree_1.types.string,
    thumbnailUrl: mobx_state_tree_1.types.string,
    version: mobx_state_tree_1.types.string
});
//# sourceMappingURL=file.js.map